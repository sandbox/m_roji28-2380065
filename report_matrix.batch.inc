<?php

function report_matrix_batch_process($data, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($data['matrix']);
    $context['sandbox']['key'] = array_keys($data['matrix']);
    $context['results']['data'] = $data;
  }
  $progress = $context['sandbox']['progress'];
  $key = $context['sandbox']['key'][$progress];  
  $function = $data['matrix'][$key]['callback'];
  $arguments = $data['matrix'][$key]['arguments'];  
  $value = '';
  if (function_exists($function) || is_callable($function)) {
    $value = $function($arguments);    
  }
  $context['results']['data']['matrix'][$key]['result'] = $value;
  // Finishing.
  $context['message'] = 'Processing data.';
  $context['sandbox']['progress']++;
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function report_matrix_batch_finished($success, $results, $operations) {  
  $data = $results['data'];  
  $cid = $data['cache id'];
  // Save to Cache.
  cache_set($cid, $data);
}
