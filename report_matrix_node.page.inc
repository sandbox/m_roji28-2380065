<?php

class ReportMatrixNode extends ReportMatrix {

  protected function build_matrix_process() {
    $matrix = &$this->data['matrix'];
    $types = node_type_get_types();
    if (isset($_GET['type']) && isset($types[$_GET['type']])) {
      $type = $_GET['type'];
    }
    else {
      $type = 'all';
    }
    foreach($matrix as $token => &$info) {
      $info += array(
        'callback' => 'report_matrix_node_process_views_report_matrix_node_default',
        'arguments' => array($info['row'], $info['column'], $type),
      );
      // Ubah menjadi link.
      $info['cell'] = array(
        'data' => array(
          '#theme' => 'link',
          '#text' => $info['cell'],
          '#path' => 'report_matrix_node_views/' . $info['row'] . '/' . $info['column'] . '/' . $type,
          '#options' => array('attributes' => array(), 'html' => FALSE),
        ),
      );
    }
  }
}

function report_matrix_node_page_default() {
  $laporan = new ReportMatrixNode();
  if (isset($_GET['current_year'])) {
    $current_year = (int) check_plain($_GET['current_year']);
    $laporan->set_options('current year', $current_year);
  }
  if (isset($_GET['number_of_years'])) {
    $number_of_years = (int) check_plain($_GET['number_of_years']);
    $laporan->set_options('number of years', $number_of_years);
  }
  // For Debug.
  // $laporan->process = FALSE;

  $laporan->execute();

  // For Debug.
  // $data = $laporan->get_data();
  // dpm($data, '$data');
  // return '';

  $result = $laporan->result;

  // For Debug.
  // $data = $result->get_data();
  // dpm($data, '$data');
  // return '';

  $result->attach_form('report_matrix_node_form_default');
  return $result->render();
}

function report_matrix_node_form_default($form, &$form_state) {
  $types = node_type_get_types();
  $options = array('' => '-');
  foreach($types as $machine_name => $object) {
    $options[$machine_name] = $object->name;
  }
  $form['type'] = array(
    '#title' => 'Pilih tipe konten',
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => isset($form_state['input']['type']) ? $form_state['input']['type'] : NULL,
  );
  $form['current_year'] = array(
    '#title' => 'Start Year',
    '#type' => 'textfield',
    '#default_value' => isset($form_state['input']['current_year']) ? $form_state['input']['current_year'] : date('Y'),
  );
  $form['number_of_years'] = array(
    '#title' => 'Range Year',
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(1,2,3,4,5,6,7,8,9,10)),
    '#default_value' => isset($form_state['input']['number_of_years']) ? $form_state['input']['number_of_years'] : 2,
  );
  return $form;
}
